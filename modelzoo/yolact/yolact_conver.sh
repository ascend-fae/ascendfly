atc --model=../model/yolact_coco_sim.onnx \
    --framework=5 \
    --output=../model/yolact_coco_sim_aipp \
    --log=debug \
    --soc_version=Ascend310 \
    --insert_op_conf=../model/yolact_coco_sim_aipp.cfg 

