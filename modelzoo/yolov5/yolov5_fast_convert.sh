 atc --model=../model/yolov5s_sim_t_with_yolo.onnx \
    --framework=5 \
    --output=../model/yolov5s_sim_t_with_yolo_aipp \
    --log=debug \
    --soc_version=Ascend310 \
    --input_shape="images:1,3,640,640" \
    --output_type=FP16 \
    --insert_op_conf=../model/yolov5s_aipp_fast.cfg \
	# --auto_tune_mode="RL,GA"
 
 
# atc --model=../model/yolov5s_sim_t_with_yolo.onnx \
#     --framework=5 \
#     --output=../model/yolov5s_sim_t_with_yolo \
#     --log=debug \
#     --input_format=NCHW \
#     --soc_version=Ascend310 \
#     --input_shape="images:1,3,640,640" \
#     --output_type=FP16 \
#    --insert_op_conf=../model/yolov5s_aipp_fast.cfg \
#    --auto_tune_mode="RL,GA"
#    # --enable_small_channel=1
#	#--input_fp16_nodes="images" \
#
