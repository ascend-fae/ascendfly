
atc --model=./yolov7.onnx \
    --framework=5 \
    --output=./yolov7_aipp \
    --log=debug \
    --soc_version=Ascend310 \
    --input_shape="images:1,3,640,640" \
    --insert_op_conf=./yolov7_aipp.cfg \    
