atc --model=../model/yolov3_8batch.prototxt \
    --weight=../model/yolov3.caffemodel \
    --framework=0 \
    --input_format=NCHW \
    --output=../model/yolov3_aipp_8batch \
    --soc_version=Ascend310 \
    --insert_op_conf=../model/yolov3_aipp.cfg 
