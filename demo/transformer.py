#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright 2020 Huawei Technologies Co., Ltd
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
import sys

sys.path.append("..")
import ascend
import numpy as np
import time

import os

from tqdm import tqdm

DTYPE = {
    'float16': np.float16,
    'float32': np.float32,
    'float64': np.float64,
    'int16': np.int16,
    'int32': np.int32,
    'int64': np.int64,
    'uint8': np.uint8,
    'uint16': np.uint16,
    'uint32': np.uint32,
    'uint64': np.uint64
}

parser.add_argument('--input_dtypes', required=True)

def load_info(info_file_path):
    # read info file from info_file_path
    inputs_info = {}
    with open(info_file_path, 'rt', encoding='utf-8') as fr:
        line = fr.readline()
        while line:
            contents = line.rstrip('\n').split()
            info = {'path': contents[1], 'shape': eval(contents[2])}
            inputs_info.setdefault(contents[0], []).append(info)
            line = fr.readline()
    return inputs_info

class Transformer():
    def __init__(self, context, model_path, confThresh=0.5, nmsThresh=0.5):
        # initial model
        self.model = ascend.AscendModel(context, model_path)

    def infer(self, input_data):
        """ do model inference.

        Args:
            input_data (AscendArray): input info.

        Returns:
            infer result (ndarray).
        """
        # feed data and run inference
        self.model.feed_data({'images':input_data})
        self.model.run()

        # get feature map of three output layer
        tensor = self.model.get_tensor_by_name('Concat_802:0').to_np
    
        return tensor


def transformer():
    device_id = 12
    model_path = "../tools/model/wav2vec2-base-960h.om"
    result_save_path = "./"

    # 解析输入类型
    input_dtypes = opt.input_dtypes.split(',')
    input_dtypes = list(map(lambda x: DTYPE[x], input_dtypes))

    for ctx in ascend.Context({device_id}):
        trans = Transformer(ctx, model_path)

        # read info 
        total_infer_num = 0
        input_info = load_info(">>>", data_type="")
        for key, values in tqdm(input_info.items()):

            inputs = []
            dims = []
            for idx, value in enumerate(values):
                x = np.fromfile(value['path'], dtype=input_dtypes[idx]).reshape(value['shape'])
                inputs.append(x)
                dims.extend(value['shape'])
            dims_info = {'dimCount': len(dims), 'name': '', 'dims': dims}

            # do infer
            output = trans.infer(inputs, dims_info)
            total_infer_num += 1

            # 保存文件
            for idx, data in enumerate(output):
                np.save(os.path.join(result_save_path, key + '.' + str(idx) + '.npy'), data)


