#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright 2020 Huawei Technologies Co., Ltd
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
import imp
import sys
import os
sys.path.append("..")
import ascend
import numpy as np
import cv2
import threading 
from queue import Queue

MODEL_WIDTH = 416
MODEL_HEIGHT = 416

class YOLOV3_8batch():
    def __init__(self, context, model_path):
        # initial model
        self.ctx = context
        self.model = ascend.AscendModel(context, model_path)
        self.classes = ascend.coco_classes()
        self.image_info = self.construct_image_info()


    def construct_image_info(self):
        """construct image info"""
        image_info = np.array([MODEL_WIDTH, MODEL_HEIGHT, 
                            MODEL_WIDTH, MODEL_HEIGHT], 
                            dtype = np.float32) 
        
        return ascend.AscendArray.clone(image_info)

    def detect(self, src_img):
        """ do model inference and detection-layer.
            @note
            the output feature-map should map with anchors.
        Args:
            src_img (AscendArray): input yuv padding image.

        Returns:
            detect result (ndarray).
        """
        in_data = ascend.imgs2tensor(self.ctx, src_img, tensor_fmt='NHWC')

        # feed data and run inference
        self.model.feed_data({'data':in_data, 'img_info':self.image_info})

        self.model.run()

        # get feature map of three output layer
        bbox_num = self.model.get_tensor_by_name('detection_out3:1:box_out_num').to_np
        bbox =self.model.get_tensor_by_name('detection_out3:0:box_out').to_np

        return bbox_num, bbox

    def post_process(self, bbox_num, bbox, shape):
        """postprocess"""
        box_num = bbox_num[0, 0]
        box_info = bbox.flatten()

        scalex = shape[1] / MODEL_WIDTH
        scaley = shape[0] / MODEL_HEIGHT
        if scalex > scaley:
            scaley =  scalex
        
        bboxes = []
        labels_id = []
        for n in range(int(box_num)):
            ids = int(box_info[5 * int(box_num) + n])
            labels_id.append(ids)
            score = box_info[4 * int(box_num)+n]
            top_left_x = box_info[0 * int(box_num)+n] * scaley
            top_left_y = box_info[1 * int(box_num)+n] * scaley
            bottom_right_x = box_info[2 * int(box_num) + n] * scaley
            bottom_right_y = box_info[3 * int(box_num) + n] * scaley
            # print("class % d, box % d % d % d % d, score % f" % (
            #     ids, top_left_x, top_left_y, 
            #     bottom_right_x, bottom_right_y, score))
            bboxes.append([top_left_x, top_left_y, bottom_right_x, bottom_right_y, score])
            
        return np.array(bboxes), np.array(labels_id)

    def __del__(self):
        del self.model

def read_frame(cap, Img, fifo):
    if cap is None:
        return

    while cap.is_open():
        yuv_img, _ = cap.read()
        if yuv_img:
            yuv_resized = Img.imrescale(yuv_img, (416, 416))
            yuv_pad = Img.impad(yuv_resized, shape=(416, 416))
            fifo.put(yuv_pad, timeout=100)
        
def infer(mdl, fifo, batch_size, shape):
    while True:
        imgs = []
        if fifo.qsize() >= batch_size:
            for _ in range(batch_size):
                imgs.append(fifo.get(timeout=30))
            
            bbox_num, bbox = mdl.detect(imgs)
            bboxes, labels = mdl.post_process(bbox_num, bbox, shape)
            print(bboxes, labels)


def yolov3_caffe_8batch():
    device_id = 1
    thread_num = 8
    model_path = "../modelzoo/yolov3/yolov3_aipp_8batch.om"
    video_stream_path = '/home/zhengguojian/ascendfly/tests/video/test-1k.264'

    ctx = ascend.Context({device_id}).context_dict[device_id]
    Img = ascend.Image(ctx)
    yolov3 = YOLOV3_8batch(ctx, model_path)
    fifo = Queue(maxsize=32)

    for i in range(thread_num):
        cap = ascend.VideoCapture(ctx, video_stream_path, channel=i)
        cap.skip_num(4)
        t_dec = threading.Thread(target=read_frame, args=(cap, Img, fifo))
        t_dec.start()

    t_infer = threading.Thread(target=infer, args=(yolov3, fifo, 8, (1920, 1080)))
    t_infer.start()
    

if __name__ == '__main__':
    yolov3_caffe_8batch()

    