#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright 2020 Huawei Technologies Co., Ltd
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
import sys
sys.path.append("..")
import ascend
import numpy as np
from itertools import product
import cv2
import math
import time
import random
COLORS = np.array([[0, 0, 0], [244, 67, 54], [233, 30, 99], [156, 39, 176], [103, 58, 183], [100, 30, 60],
                    [63, 81, 181], [33, 150, 243], [3, 169, 244], [0, 188, 212], [20, 55, 200],
                    [0, 150, 136], [76, 175, 80], [139, 195, 74], [205, 220, 57], [70, 25, 100],
                    [255, 235, 59], [255, 193, 7], [255, 152, 0], [255, 87, 34], [90, 155, 50],
                    [121, 85, 72], [158, 158, 158], [96, 125, 139], [15, 67, 34], [98, 55, 20],
                    [21, 82, 172], [58, 128, 255], [196, 125, 39], [75, 27, 134], [90, 125, 120],
                    [121, 82, 7], [158, 58, 8], [96, 25, 9], [115, 7, 234], [8, 155, 220],
                    [221, 25, 72], [188, 58, 158], [56, 175, 19], [215, 67, 64], [198, 75, 20],
                    [62, 185, 22], [108, 70, 58], [160, 225, 39], [95, 60, 144], [78, 155, 120],
                    [101, 25, 142], [48, 198, 28], [96, 225, 200], [150, 167, 134], [18, 185, 90],
                    [21, 145, 172], [98, 68, 78], [196, 105, 19], [215, 67, 84], [130, 115, 170],
                    [255, 0, 255], [255, 255, 0], [196, 185, 10], [95, 167, 234], [18, 25, 190],
                    [0, 255, 255], [255, 0, 0], [0, 255, 0], [0, 0, 255], [155, 0, 0],
                    [0, 155, 0], [0, 0, 155], [46, 22, 130], [255, 0, 155], [155, 0, 255],
                    [255, 155, 0], [155, 255, 0], [0, 155, 255], [0, 255, 155], [18, 5, 40],
                    [120, 120, 255], [255, 58, 30], [60, 45, 60], [75, 27, 244], [128, 25, 70]], dtype='uint8')


def draw_img(ids_p, class_p, box_p, mask_p, img_origin, img_name=None, fps=None, COLORS=None, class_names=None,
            hide_mask=False, hide_bbox=False, hide_score=False, cutout=False, real_time=False, num_classes=81):
    if ids_p is None:
        return img_origin
    num_detected = ids_p.shape[0]
    img_fused = img_origin
    if not hide_mask:  # 显示mask
        masks_semantic = mask_p * (ids_p[:, None, None] + 1)  # expand ids_p' shape for broadcasting
        # The color of the overlap area is different because of the '%' operation.
        masks_semantic = masks_semantic.astype('int').sum(axis=0) % (num_classes - 1)
        color_masks = COLORS[masks_semantic].astype('uint8')
        img_fused = cv2.addWeighted(color_masks, 0.4, img_origin, 0.6, gamma=0)

        if cutout:
            try:
                for i in range(num_detected):
                    one_obj = np.tile(mask_p[i], (3, 1, 1)).transpose((1, 2, 0))
                    one_obj = one_obj * img_origin
                    new_mask = mask_p[i] == 0
                    new_mask = np.tile(new_mask * 255, (3, 1, 1)).transpose((1, 2, 0))
                    x1, y1, x2, y2 = box_p[i, :]
                    # print("x1, y1, x2, y2: ", x1, y1, x2, y2)
                    img_matting = (one_obj + new_mask)[y1:y2, x1:x2, :]
                    cv2.imwrite(f'results/images/{img_name}_{i}.jpg', img_matting)
            except:
                print("Error img_name: ", img_name)
                pass

    scale = 0.6
    thickness = 1
    font = cv2.FONT_HERSHEY_DUPLEX

    if not hide_bbox:
        for i in reversed(range(num_detected)):
            x1, y1, x2, y2 = box_p[i, :]

            color = COLORS[ids_p[i] + 1].tolist()
            cv2.rectangle(img_fused, (x1, y1), (x2, y2), color, thickness)

            class_name = class_names[ids_p[i]]
            text_str = f'{class_name}: {class_p[i]:.2f}' if not hide_score else class_name

            text_w, text_h = cv2.getTextSize(text_str, font, scale, thickness)[0]
            cv2.rectangle(img_fused, (x1, y1), (x1 + text_w, y1 + text_h + 5), color, -1)
            cv2.putText(img_fused, text_str, (x1, y1 + 15), font, scale, (255, 255, 255), thickness, cv2.LINE_AA)

    if real_time:
        fps_str = f'fps: {fps:.2f}'
        text_w, text_h = cv2.getTextSize(fps_str, font, scale, thickness)[0]
        # Create a shadow to show the fps more clearly
        img_fused = img_fused.astype(np.float32)
        img_fused[0:text_h + 8, 0:text_w + 8] *= 0.6
        img_fused = img_fused.astype(np.uint8)
        cv2.putText(img_fused, fps_str, (0, text_h + 2), font, scale, (255, 255, 255), thickness, cv2.LINE_AA)

    return img_fused


class YOLACT():
    def __init__(self, context, model_path, scorefThresh=0.3, nmsThresh=0.3, topk=100, max_detects=100):
        # initial model
        self.model = ascend.AscendModel(context, model_path)
        self.classes = ascend.coco_classes()

        self.anchors = []
        self.top_k = topk
        self.max_detects = max_detects
        self.nms_score_th = scorefThresh
        self.nms_iou_th = nmsThresh
        self.visual_thre = 0.3
        self.no_crop = False
        self.aspect_ratios = [1, 1 / 2, 2]
        self.gen_anchor()
        # self.norm_mean = np.array([103.94, 116.78, 123.68], dtype=np.float32)
        self.norm_mean = np.array([104, 117, 124], dtype=np.float32)
        self.norm_std = np.array([57.38, 57.12, 58.40], dtype=np.float32)

    def make_anchors(self, conv_h, conv_w, scale):
        prior_data = []
        # Iteration order is important (it has to sync up with the convout)
        for j, i in product(range(conv_h), range(conv_w)):
            # + 0.5 because priors are in center
            x = (i + 0.5) / conv_w
            y = (j + 0.5) / conv_h

            for ar in self.aspect_ratios:
                ar = math.sqrt(ar)
                w = scale * ar / 550
                h = scale / ar / 550

                prior_data += [x, y, w, h]

        return prior_data

    def gen_anchor(self):
        scale = [24, 48, 96, 192, 384]
        fpn_fm_shape = [math.ceil(550 / stride) for stride in (8, 16, 32, 64, 128)]
        for i, size in enumerate(fpn_fm_shape):
            self.anchors += self.make_anchors(size, size, scale[i])


    def box_iou_numpy(self, box_a, box_b):
        (n, A), B = box_a.shape[:2], box_b.shape[1]
        # add a dimension
        box_a = np.tile(box_a[:, :, None, :], (1, 1, B, 1))
        box_b = np.tile(box_b[:, None, :, :], (1, A, 1, 1))

        max_xy = np.minimum(box_a[..., 2:], box_b[..., 2:])
        min_xy = np.maximum(box_a[..., :2], box_b[..., :2])
        inter = np.clip((max_xy - min_xy), a_min=0, a_max=100000)
        inter_area = inter[..., 0] * inter[..., 1]

        area_a = (box_a[..., 2] - box_a[..., 0]) * (box_a[..., 3] - box_a[..., 1])
        area_b = (box_b[..., 2] - box_b[..., 0]) * (box_b[..., 3] - box_b[..., 1])

        return inter_area / (area_a + area_b - inter_area)

    def nms_numpy(self, class_pred, box_pred, coef_pred, proto_out):
        class_p = class_pred.squeeze()  # [19248, 81]
        box_p = box_pred.squeeze()  # [19248, 4]
        coef_p = coef_pred.squeeze()  # [19248, 32]
        proto_p = proto_out.squeeze()  # [138, 138, 32]
        anchors = np.array(self.anchors).reshape(-1, 4)  # add

        class_p = class_p.transpose(1, 0)
        # exclude the background class
        class_p = class_p[1:, :]
        # get the max score class of 19248 predicted boxes

        class_p_max = np.max(class_p, axis=0)  # [19248]

        # filter predicted boxes according the class score
        keep = (class_p_max > self.nms_score_th)
        class_thre = class_p[:, keep]

        box_thre, anchor_thre, coef_thre = box_p[keep, :], anchors[keep, :], coef_p[keep, :]

        # decode boxes
        box_thre = np.concatenate((anchor_thre[:, :2] + box_thre[:, :2] * 0.1 * anchor_thre[:, 2:],
                                anchor_thre[:, 2:] * np.exp(box_thre[:, 2:] * 0.2)), axis=1)
        box_thre[:, :2] -= box_thre[:, 2:] / 2
        box_thre[:, 2:] += box_thre[:, :2]

        if class_thre.shape[1] == 0:
            return None, None, None, None, None
        else:
            box_thre, coef_thre, class_ids, class_thre = self.fast_nms_numpy(box_thre, coef_thre, class_thre)
            return class_ids, class_thre, box_thre, coef_thre, proto_p

    def fast_nms_numpy(self, box_thre, coef_thre, class_thre):
        # descending sort
        idx = np.argsort(-class_thre, axis=1)
        class_thre = np.sort(class_thre, axis=1)[:, ::-1]

        idx = idx[:, :self.top_k]
        class_thre = class_thre[:, :self.top_k]

        num_classes, num_dets = idx.shape
        box_thre = box_thre[idx.reshape(-1), :].reshape(num_classes, num_dets, 4)  # [80, 64, 4]
        coef_thre = coef_thre[idx.reshape(-1), :].reshape(num_classes, num_dets, -1)  # [80, 64, 32]

        iou = self.box_iou_numpy(box_thre, box_thre)
        iou = np.triu(iou, k=1)
        iou_max = np.max(iou, axis=1)

        # Now just filter out the ones higher than the threshold
        keep = (iou_max <= self.nms_iou_th)

        # Assign each kept detection to its corresponding class
        class_ids = np.tile(np.arange(num_classes)[:, None], (1, keep.shape[1]))

        class_ids, box_nms, coef_nms, class_nms = class_ids[keep], box_thre[keep], coef_thre[keep], class_thre[keep]

        # Only keep the top args.max_num_detections highest scores across all classes
        idx = np.argsort(-class_nms, axis=0)
        class_nms = np.sort(class_nms, axis=0)[::-1]

        idx = idx[:self.max_detects]
        class_nms = class_nms[:self.max_detects]

        class_ids = class_ids[idx]
        box_nms = box_nms[idx]
        coef_nms = coef_nms[idx]

        return box_nms, coef_nms, class_ids, class_nms

    def sanitize_coordinates_numpy(self, _x1, _x2, img_size, padding=0):
        _x1 = _x1 * img_size
        _x2 = _x2 * img_size

        x1 = np.minimum(_x1, _x2)
        x2 = np.maximum(_x1, _x2)
        x1 = np.clip(x1 - padding, a_min=0, a_max=1000000)
        x2 = np.clip(x2 + padding, a_min=0, a_max=img_size)

        return x1, x2

    def crop_numpy(self, masks, boxes, padding=1):
        h, w, n = masks.shape
        x1, x2 = self.sanitize_coordinates_numpy(boxes[:, 0], boxes[:, 2], w, padding)
        y1, y2 = self.sanitize_coordinates_numpy(boxes[:, 1], boxes[:, 3], h, padding)

        rows = np.tile(np.arange(w)[None, :, None], (h, 1, n))
        cols = np.tile(np.arange(h)[:, None, None], (1, w, n))

        masks_left = rows >= (x1.reshape(1, 1, -1))
        masks_right = rows < (x2.reshape(1, 1, -1))
        masks_up = cols >= (y1.reshape(1, 1, -1))
        masks_down = cols < (y2.reshape(1, 1, -1))

        crop_mask = masks_left * masks_right * masks_up * masks_down

        return masks * crop_mask

    def after_nms_numpy(self, ids_p, class_p, box_p, coef_p, proto_p, img_h, img_w):
        def np_sigmoid(x):
            return 1 / (1 + np.exp(-x))

        if ids_p is None:
            return None, None, None, None

        if  self.visual_thre > 0:
            keep = class_p >= self.visual_thre
            if not keep.any():
                return None, None, None, None

            ids_p = ids_p[keep]
            class_p = class_p[keep]
            box_p = box_p[keep]
            coef_p = coef_p[keep]

        masks = np_sigmoid(np.matmul(proto_p, coef_p.T))

        if not self.no_crop:  # Crop masks by box_p
            masks = self.crop_numpy(masks, box_p)

        ori_size = max(img_h, img_w)
        masks = cv2.resize(masks, (ori_size, ori_size), interpolation=cv2.INTER_LINEAR)

        if masks.ndim == 2:
            masks = masks[:, :, None]

        masks = np.transpose(masks, (2, 0, 1))
        masks = masks > 0.5  # Binarize the masks because of interpolation.
        masks = masks[:, 0: img_h, :] if img_h < img_w else masks[:, :, 0: img_w]

        box_p *= ori_size
        box_p = box_p.astype('int32')

        return ids_p, class_p, box_p, masks

    def infer(self, image):
        # feed data and run inference
        self.model.feed_data({'input.1':image})
        
        self.model.run()

        # get feature map of three output layer
        class_p = self.model.get_tensor_by_name('Softmax_325:0').to_np # (1, 19248, 81) class_p
        box_p   = self.model.get_tensor_by_name('Concat_323:0').to_np # (1, 19248, 4) box_p
        coef_p  = self.model.get_tensor_by_name('Concat_324:0').to_np # (1, 19248, 32) coef_p
        proto_p = self.model.get_tensor_by_name('Transpose_186:0').to_np # (1, 138, 138, 32) proto_p

        return class_p, box_p, coef_p, proto_p


    def pad_to_square(self, img, masks=None, boxes=None, during_training=False):
        img_h, img_w = img.shape[:2]
        if img_h == img_w:
            return (img, masks, boxes) if during_training else img
        else:
            pad_size = max(img_h, img_w)
            pad_img = np.zeros((pad_size, pad_size, 3), dtype='float32')
            pad_img[:, :, :] = self.norm_mean

            if during_training:
                pad_masks = np.zeros((masks.shape[0], pad_size, pad_size), dtype='float32')

                if img_h < img_w:
                    random_y1 = random.randint(0, img_w - img_h)
                    pad_img[random_y1: random_y1 + img_h, :, :] = img
                    pad_masks[:, random_y1: random_y1 + img_h, :] = masks
                    boxes[:, [1, 3]] += random_y1

                if img_h > img_w:
                    random_x1 = random.randint(0, img_h - img_w)
                    pad_img[:, random_x1: random_x1 + img_w, :] = img
                    pad_masks[:, :, random_x1: random_x1 + img_w] = masks
                    boxes[:, [0, 2]] += random_x1

                return pad_img, pad_masks, boxes
            else:
                pad_img[0: img_h, 0: img_w, :] = img
                return pad_img

    def multi_scale_resize(self, img, masks=None, boxes=None, resize_range=None, during_training=False):
        assert img.shape[0] == img.shape[1], 'Error, image is not square in <multi_scale_resize>'

        if during_training:
            ori_size = img.shape[0]
            resize_size = random.randint(resize_range[0], resize_range[1]) * 32
            img = cv2.resize(img, (resize_size, resize_size))
            scale = resize_size / ori_size
            boxes *= scale

            masks = masks.transpose((1, 2, 0))
            masks = cv2.resize(masks, (resize_size, resize_size))

            # OpenCV resizes a (w,h,1) array to (s,s), so fix it
            if len(masks.shape) == 2:
                masks = np.expand_dims(masks, 0)
            else:
                masks = masks.transpose((2, 0, 1))

            return img, masks, boxes
        else:
            return cv2.resize(img, (resize_range, resize_range))

    def normalize_and_toRGB(self, img):
        img = (img - self.norm_mean) / self.norm_std
        img = img[:, :, (2, 1, 0)]
        img = np.transpose(img, (2, 0, 1))
        return img

    def val_aug(self, img, val_size):
        img = img.astype('float32')
        img = self.pad_to_square(img, during_training=False)
        img = self.multi_scale_resize(img, resize_range=val_size, during_training=False)
        # img_u8 = img.astype('uint8')
        # cv2.imshow('aa', img_u8)
        # cv2.waitKey()
        img = self.normalize_and_toRGB(img)
        return img

    def __del__(self):
        del self.model

def yolov5_detection():
    device_id = 12
    # model_path = "../tools/model/yolact_coco_sim_aipp.om"
    model_path = "../modelzoo/yolact/yolact_coco_sim.om"
    video_stream_path = '/home/zhengguojian/ascendfly/tests/video/test-1k.264'

    ctx = ascend.Context({device_id}).context_dict[device_id]
    Img = ascend.Image(ctx)
    cap = ascend.VideoCapture(ctx, video_stream_path)
    yolact = YOLACT(ctx, model_path)

    # origin_img = cv2.imread('../tests/image/girl1.jpg')
    # img = yolact.val_aug(origin_img, 550)    # 测试时数据增强
    # # print("shape: ", img.shape)
    # onnx_input = img[np.newaxis, :]
    # yuv_pad = ascend.AscendArray.clone(onnx_input)

    while cap.is_open():
        yuv_img, frame_id = cap.read()
        if yuv_img:
            yuv_resized = Img.imrescale(yuv_img, (550, 550))
            origin_img = cv2.cvtColor(yuv_resized.to_np, cv2.COLOR_YUV2RGB_NV21)
            img = yolact.val_aug(origin_img, 550)    # 测试时数据增强
            # print("shape: ", img.shape)
            onnx_input = img[np.newaxis, :]
            yuv_pad = ascend.AscendArray.clone(onnx_input)

            t1 = time.time()
            class_p, box_p, coef_p, proto_p = yolact.infer(yuv_pad)

            ids_p, class_p, box_p, coef_p, proto_p = yolact.nms_numpy(class_p, box_p, coef_p, proto_p)
            ids_p, class_p, boxes_p, masks_p = yolact.after_nms_numpy(ids_p, class_p, box_p, coef_p, proto_p, origin_img.shape[0], origin_img.shape[1])
            print("elapse: ", time.time() - t1)

            img_numpy = draw_img(ids_p, class_p, boxes_p, masks_p, origin_img, img_name='img_name', COLORS=COLORS, class_names=yolact.classes)
            cv2.namedWindow('result')
            cv2.imshow('result', img_numpy)
            cv2.waitKey(5)
            

if __name__ == '__main__':
    yolov5_detection()