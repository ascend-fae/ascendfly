#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright 2020 Huawei Technologies Co., Ltd
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
import sys

sys.path.append("..")
import ascend
import numpy as np
import cv2
import time


class YOLOV5s_Fast():
    def __init__(self, context, model_path, confThresh=0.45, nmsThresh=0.45):
        # initial model
        self.model = ascend.AscendModel(context, model_path)
        self.classes = ascend.coco_classes()

        self.layer_num = 3
        self.top_k = 300  # maximum number of detections per image
        self.no = 85

        self.conf_th = confThresh
        self.nms_th = nmsThresh

    def detect(self, src_img):
        """ do model inference and detection-layer.
            @note
            the output feature-map should map with anchors.
        Args:
            src_img (AscendArray): input yuv padding image.

        Returns:
            detect result (ndarray).
        """
        # feed data and run inference
        self.model.feed_data({'images':src_img})

        self.model.run()

        # get feature map of three output layer
        tensor = self.model.get_tensor_by_name('Concat_802:0:output').to_np
    
        return tensor


    def scale_coords(self, mdl_shape, coords, img_shape):
        # Rescale coords (xyxy) from img1_shape to img0_shape
        scale = max(img_shape[0] / mdl_shape[0], img_shape[1] / mdl_shape[1]) 
        coords[:, :4] *= scale

        # Clip bounding xyxy bounding boxes to image shape (height, width)
        coords[:, 0] = coords[:, 0].clip(0, img_shape[1])  # x1
        coords[:, 1] = coords[:, 1].clip(0, img_shape[0])  # y1
        coords[:, 2] = coords[:, 2].clip(0, img_shape[1])  # x2
        coords[:, 3] = coords[:, 3].clip(0, img_shape[0])  # y2
        return coords


    def post_process(self, pred, mdl_shape, image_shape):
        conf_mask = pred[..., 4] > self.conf_th  # candidates
        pred = pred[conf_mask]

        prd_boxes = pred[:, :4]
        # compute conf with method: conf = obj_conf * cls_conf
        obj_score = pred[:, 4:5] * pred[:, 5:]
        num_class = obj_score.shape[-1]

        # Process detections
        res = []
        for i in range(num_class):  # detections per image
            id = np.nonzero(obj_score[:, i] >= self.conf_th)
            
            boxes = prd_boxes[id]
            if len(boxes) <= 0:
                continue

            scores = obj_score[:, i][id]
            idx = ascend.nms(boxes, scores, self.nms_th)[:self.top_k]
            label = np.ones(len(idx), dtype='float32') * i

            # Rescale boxes from img_size to im0 size
            boxes = self.scale_coords(mdl_shape, boxes[idx], image_shape).round()
            res.append(np.concatenate((boxes, scores[idx, None], label[:, None]), 1))
        
        if res != []:
            res = np.concatenate(res, axis=0)
        else:
            res = None
        return res
        

    def __del__(self):
        del self.model




def yolov5_fast_det():
    device_id = 1
    model_path = "../modelzoo/yolov5/yolov5s_sim_t_with_yolo_aipp.om"
    video_stream_path = '/home/zhengguojian/ascendfly/tests/video/test-1k.264'

    ctx = ascend.Context({device_id}).context_dict[device_id]
    Img = ascend.Image(ctx)
    cap = ascend.VideoCapture(ctx, video_stream_path, channel=1)
    # cap.skip_frame('NONKEY')
    cap.skip_num(5)
    # # cap.skip_frame('ALL')
    # cap.skip_frame('NONREF')

    yolofast = YOLOV5s_Fast(ctx, model_path)
    
    while cap.is_open():
        yuv_img, _ = cap.read()
        if yuv_img:
            t1 = time.time()
            yuv_resized = Img.imrescale(yuv_img, (640, 640))
            yuv_pad = Img.impad(yuv_resized, shape=(640, 640))

            origin_img = cv2.cvtColor(yuv_resized.to_np, cv2.COLOR_YUV2RGB_NV21)

            pred = yolofast.detect(yuv_pad)
            bboxes = yolofast.post_process(pred, (640, 640), origin_img.shape[:2])
            print("e2e: ", time.time() - t1)
            
            ascend.draw_bboxes(origin_img, bboxes, ascend.coco_classes(), win_name="video1",wait_time=20)
            # Img.imwrite(yuv_img, f"./result/save_{t1}.png")

if __name__ == '__main__':
    yolov5_fast_det()
    
