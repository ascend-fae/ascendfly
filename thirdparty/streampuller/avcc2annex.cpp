#include <stdio.h>
#include <time.h>
#include <string>
#include <pybind11/pybind11.h>

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
}

#define LOG(message) \
    time_t t = time(NULL); \ 
    tm *_tm = localtime(&t); \
    printf("[ERROR] %04d-%02d-%02d-%02d-%02d-%02d %s @line:%d, %s.\n", _tm->tm_year + 1900, _tm->tm_mon+1, _tm->tm_mday, _tm->tm_hour, _tm->tm_min, _tm->tm_sec, __FILE__, __LINE__, message);



const AVBitStreamFilter *pFilter = av_bsf_get_by_name("h264_mp4toannexb");

int avcc2annex(AVFormatContext *pFormatCtx_, AVPacket *pkt, int videoStream) 
{
    AVBSFContext *pBsfCtx_ = nullptr; 
    int ret = av_bsf_alloc(pFilter, &pBsfCtx_);
    if (ret != 0) {
        LOG("Alloc bsf failed!");
        return -1;
    }

    ret = avcodec_parameters_from_context(pBsfCtx_->par_in, pFormatCtx_->streams[videoStream]->codec);
    if (ret < 0) {
        LOG("Set Codec failed!");
        return -1;
    }

    ret = av_bsf_init(pBsfCtx_);
    if (ret < 0) {
        LOG("Init bsf failed!");
        return -1;
    }

    av_bsf_send_packet(pBsfCtx_, pkt);
    ret = av_bsf_receive_packet(pBsfCtx_, pkt);
    if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
        return -1;
    else if (ret < 0) {
        LOG("Receive packet failed!");
        return -1;
    }
    return 0;
}


PYBIND11_MODULE(avcc2annex, m)
{
    m.doc() = "Define a function to modify packet and transmit video format from avcc to annex-b.";
    py::class_<avcc2annex>(m, "avcc2annex")
        .def(py::init<std::string, int>())
}