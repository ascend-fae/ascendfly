
#include <pybind11/pybind11.h>
#include "StreamPuller.h"

namespace py = pybind11;

namespace {
const int LOW_THRESHOLD = 128;
const int MAX_THRESHOLD = 4096;
}

namespace {
    const int H265_MAIN_LEVEL = 0;
    const int H264_MAIN_LEVEL = 1;
}


StreamPuller::StreamPuller(std::string streamName, int channel) {
    streamName_ = streamName;
    channelId_ = channel;
    Init();
}
StreamPuller::~StreamPuller() {
    DeInit();
}

int StreamPuller::Init() 
{
    av_register_all();

    // init network
    avformat_network_init();

    // create context
    if ((pFormatCtx_ = CreateFormatContext()) == nullptr) {
        LOG("pFormatCtx_ null!");
        return -1;
    }
    
    // dump video info
	av_dump_format(pFormatCtx_, 0, streamName_.c_str(), 0); 

    // get stream infomation
    int ret = GetStreamInfo();
    if (ret != 0) {
        LOG("Stream Info Check failed.");
        return -1;
    }
   
    return 0;
}

int StreamPuller::GetStreamInfo()
{
    if (pFormatCtx_ != nullptr) {
        videoStream_ = -1;
        frameInfo_.frameId = 0;
        frameInfo_.channelId = channelId_;
        AVCodecID codecId = pFormatCtx_->streams[0]->codecpar->codec_id;
        if (codecId == AV_CODEC_ID_H264) {
            frameInfo_.format = H264_MAIN_LEVEL;
        } else if (codecId == AV_CODEC_ID_H265) {
            frameInfo_.format = H265_MAIN_LEVEL;
        } else {
            transAvcc = true;
            frameInfo_.format = H264_MAIN_LEVEL;
        }
        
        for (unsigned int i = 0; i < pFormatCtx_->nb_streams; i++) {
            AVStream *inStream = pFormatCtx_->streams[i];
            if (inStream->codecpar->codec_type == AVMEDIA_TYPE_VIDEO) {
                videoStream_ = i;
                frameInfo_.height = inStream->codecpar->height;
                frameInfo_.width = inStream->codecpar->width;
                break;
            }
        }

        if (videoStream_ == -1) {
            LOG("Cann't find a video stream!");
            return -1;
        }

        if (frameInfo_.height < LOW_THRESHOLD || frameInfo_.width < LOW_THRESHOLD ||
            frameInfo_.height > MAX_THRESHOLD || frameInfo_.width > MAX_THRESHOLD) {
            LOG("Size of frame is not supported in DVPP Video Decode!");
            return -1;
        }
    }
    return 0;
}


AVFormatContext *StreamPuller::CreateFormatContext()
{
    // create message for stream pull
    AVFormatContext *formatContext = nullptr;
    AVDictionary *options = nullptr;
    av_dict_set(&options, "rtsp_transport", "tcp", 0);
    av_dict_set(&options, "stimeout", "3000000", 0);
    int ret = avformat_open_input(&formatContext, streamName_.c_str(), nullptr, &options);
    if (options != nullptr) {
        av_dict_free(&options);
    }
    if (ret != 0) {
        LOG("Couldn't open input stream.");
        return nullptr;
    }

    ret = avformat_find_stream_info(formatContext, nullptr);
    if (ret != 0) {
        LOG("Couldn't find stream information.");
        return nullptr;
    }
    return formatContext;
}

int StreamPuller::Process(Frame **iframe) 
{
    AVPacket pkt;
    av_init_packet(&pkt);
    int ret = av_read_frame(pFormatCtx_, &pkt);
    if (ret != 0) {
        if (ret == AVERROR_EOF) {
            LOG("StreamPuller this channel StreamPuller is EOF, exit");
            std::shared_ptr<Frame> frame = std::make_shared<Frame>();
            frame->frameInfo = frameInfo_;
            frame->frameInfo.eof = true;

            *iframe = frame.get();
            return -1;
        }
        av_packet_unref(&pkt);
        return 0;
    } 
    else if (pkt.stream_index == videoStream_) {
        if (pkt.size <= 0) {
            LOG("Invalid pkt.size.");
            av_packet_unref(&pkt);
            return 0;
        }

        // If input avcc format stream, transmit video format to annex-b.
        if (transAvcc && ((ret = Avcc2annex(&pkt)) != 0)) {
            return ret;
        }

        std::shared_ptr<Frame> frame = std::make_shared<Frame>();
        uint8_t* dataBuffer = (uint8_t *)malloc(pkt.size);
        frame->data.reset(dataBuffer, free);
        std::copy(pkt.data, pkt.data + pkt.size, dataBuffer);
        frame->frameInfo = frameInfo_;
        frame->frameInfo.eof = false;
        frame->size = pkt.size;

        frameInfo_.frameId++;

        *iframe = frame.get();
        // printf("Write Video Packet. size:%d\tpts:%lld\n",pkt.size,pkt.pts);   
    }
    av_packet_unref(&pkt);
    
    return 0;
}

int StreamPuller::Avcc2annex(AVPacket *pkt) 
{
    int ret = av_bsf_alloc(pFilter, &pBsfCtx_);
    if (ret != 0) {
        LOG("Alloc bsf failed!");
        return -1;
    }

    ret = avcodec_parameters_from_context(pBsfCtx_->par_in, pFormatCtx_->streams[videoStream_]->codec);
    if (ret < 0) {
        LOG("Set Codec failed!");
        return -1;
    }

    ret = av_bsf_init(pBsfCtx_);
    if (ret < 0) {
        LOG("Init bsf failed!");
        return -1;
    }

    av_bsf_send_packet(pBsfCtx_, pkt);
    ret = av_bsf_receive_packet(pBsfCtx_, pkt);
    if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
        return -1;
    else if (ret < 0) {
        LOG("Receive packet failed!");
        return -1;
    }
    return 0;
}

int StreamPuller::DeInit() {
    // close video stream
    if (pFormatCtx_ != nullptr) {
        avformat_close_input(&pFormatCtx_);
        pFormatCtx_ = nullptr;
    }

    if (pBsfCtx_ != nullptr) {
        av_bsf_free(&pBsfCtx_);
        pBsfCtx_ = nullptr;
    }
    return 0;
}

/*
int main() {
    const char *in_filename = "/home/zhengguojian/ascendfly/tests/video/zhongchuliang_001.mp4";
    const char *out_filename_v = "ffmpeg_demo.h264";
    FILE *fp_video=fopen(out_filename_v, "wb+");
    StreamPuller stream_pull = StreamPuller(in_filename, 0);

    // malloc
    // std::shared_ptr<Frame> frame = std::make_shared<Frame>();
    // uint8_t* dataBuffer = (uint8_t *)malloc(2*1024*1024);
    // frame->data.reset(dataBuffer, free);

    Frame *frame = nullptr;
    while (1) {
        int ret = stream_pull.Process(&frame);
        if (ret != 0) break;
        fwrite(frame->data.get(), 1, frame->size, fp_video);
    }
    fclose(fp_video);
}
*/

PYBIND11_MODULE(StreamPuller, m)
{
    m.doc() = "Define a StreamPuller class to pull video stream and unpack the packet data.";
    py::class_<StreamPuller>(m, "StreamPuller")
        .def(py::init<std::string, int>())
        .def("Init", &StreamPuller::Init)
        .def("Denit", &StreamPuller::DeInit)
        .def("Process", &StreamPuller::Process);
    /*
    py::class_<Frame>(m, "Frame")
        .def(py::init())
        .def_readwrite("frameInfo", &Frame::frameInfo)
        .def_readwrite("size", &Frame::size)
        .def_readwrite("data", &Frame::data);

    py::class_<FrameInfo>(m, "FrameInfo")
        .def(py::init())
        .def_readwrite("eof", &Frame::eof)
        .def_readwrite("channelId", &Frame::channelId)
        .def_readwrite("frameId", &Frame::frameId)
        .def_readwrite("width", &Frame::width)
        .def_readwrite("height", &Frame::height)
        .def_readwrite("format", &Frame::format);
        */
}

/*
struct FrameInfo {
    bool eof;
    uint32_t channelId;
    uint64_t frameId;
    uint32_t width;
    uint32_t height;
    int format;
};

struct Frame {
    FrameInfo frameInfo;
    size_t size; // Size of memory, bytes
    std::shared_ptr<void> data; // Smart pointer of data
};
*/