import av
import av.datasets
import io
# from av.bitstream import BitStreamFilter, BitStreamFilterContext

# input_ = av.open(av.datasets.curated('/home/zhengguojian/ascendfly/tests/video/zhongchuliang_001.mp4'))

stream_path = '/home/zhengguojian/ascendfly/tests/video/zhongchuliang_001.mp4'
dicOption={'buffer_size':'1024000', 'rtsp_transport':'tcp', 'stimeout':'20000000', 'max_delay':'200000'}
container = av.open(stream_path, 'r', format=None, options=dicOption, metadata_errors='nostrict')

decfile = open('night-sky.h264', 'wb')

in_stream = container.streams.video[0]
bsf = av.BitStreamFilterContext('h264_mp4toannexb')

byte_io = io.BytesIO()
byte_io.name = 'muxed.h264'
out_container = av.open(byte_io, 'wb')
out_stream = out_container.add_stream(template=in_stream)

for packet in container.demux(in_stream):
    if packet.dts is None:
        continue
    for out_packet in bsf(packet):
        out_container.mux_one(out_packet)
        pkt = out_container.demux(out_stream)
        decfile.write(pkt)

'''
import av
import io
from av.bitstream import BitStreamFilter, BitStreamFilterContext

# import PyNvCodec as nvc
import numpy as np
import sys


def decode_h264(input_file, output_file):
    dec_file = open(output_file, "wb+")
    input_container = av.open(input_file)
    in_stream = input_container.streams.video[0]
    bsfc = BitStreamFilterContext('h264_mp4toannexb')

    #Create raw byte IO instead of file IO
    #Fake the extension to satisfy FFmpeg muxer
    byte_io = io.BytesIO()
    byte_io.name = 'muxed.h264'

    #Make FFmpeg to output Annex.B H.264 packets to raw bytes IO
    out_container = av.open(byte_io, 'wb')
    out_stream = out_container.add_stream(template=in_stream)

    for packet in input_container.demux(in_stream):
        print("ssss")
        
        for out_packet in bsfc(packet):
            out_container.mux_one(out_packet)
            byte_io.flush()
            enc_packet = np.frombuffer(buffer=byte_io.getvalue(), dtype=np.uint8)
            
            dec_file.write(enc_packet)

            byte_io.seek(0)
            byte_io.truncate()
            

    input_container.close()
    dec_file.close()
    out_container.close()

    return 0


def main():
    decode_h264("/home/zhengguojian/ascendfly/tests/video/zhongchuliang_001.mp4", "./1080p_30fps_normal.nv12")
    return 0


if __name__ == "__main__":
    main()
'''