#ifndef INC_STREAMPULLER_H
#define INC_STREAMPULLER_H
#include <stdio.h>
#include <time.h>
#include <string>
#include <memory>

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
}

#define LOG(message) \
    time_t t = time(NULL); \ 
    tm *_tm = localtime(&t); \
    printf("[ERROR] %04d-%02d-%02d-%02d-%02d-%02d %s @line:%d, %s.\n", _tm->tm_year + 1900, _tm->tm_mon+1, _tm->tm_mday, _tm->tm_hour, _tm->tm_min, _tm->tm_sec, __FILE__, __LINE__, message);


struct FrameInfo {
    bool eof;
    uint32_t channelId;
    uint64_t frameId;
    uint32_t width;
    uint32_t height;
    int format;
};

struct Frame {
    FrameInfo frameInfo;
    size_t size; // Size of memory, bytes
    std::shared_ptr<void> data; // Smart pointer of data
};


class StreamPuller {
public:
    StreamPuller(std::string streamName, int channel);
    virtual ~StreamPuller();

    int Init();
    int DeInit(void);

    int Process(Frame **iframe);

private:
    AVFormatContext *CreateFormatContext();
    int GetStreamInfo();
    int Avcc2annex(AVPacket *pkt);

private:
    // unpacked frame info
    FrameInfo frameInfo_;

    // stream name or rtsp address
    std::string streamName_;
 
    AVFormatContext *pFormatCtx_ = nullptr;
    AVBSFContext *pBsfCtx_ = nullptr; 
    const AVBitStreamFilter *pFilter = av_bsf_get_by_name("h264_mp4toannexb");

    // flag of avcc transmit to annnex-b format
    bool transAvcc = false;
    int videoStream_ = -1;
    int channelId_ = -1;
};

#endif
